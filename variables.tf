
variable username {
    description = "User name"
    type        = string
}
variable "userpass" {
    description = "User password"
    type        = string
}
variable mail {
    description = "User email"
    type        = string
}
